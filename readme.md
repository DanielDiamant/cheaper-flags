# Cheaper Flags
A zero-dependency, framework agnostic, local-storage based feature flags (toggles) solution for web apps.


## Install
    npm i cheaper-flags


## Initiate
```typescript
import { FeaturesManager, ConsoleClient } from 'cheaper-flags';

const defaultState = {
    feature1: false,
    feature2: false,
    feature3: true,
};
const featuresManager = new FeaturesManager(defaultState);
new ConsoleClient(featuresManager);
```

## Usage
```typescript
const state = featuresManager.getState();
if (state.feature1) {
    // do something
}
```

## Manage flags with ConsoleClient

When creating a new instance of ConsoleClient:
```typescript
new ConsoleClient(featuresManager);
```
A new global variable is being set: `window.ff`.

This variable is accessible from the devtools console, just by typing `ff`.

### ff.status()
Show the current status of the flags
```
ff.status()

// will log out:
    feature1	 on (orioginal: off) 
    feature2	 off
    feature3	 on
```

### ff.toggles
This is where you can control your features:
```
ff.toggles.feature1.on()
ff.toggles.feature1.off()
ff.toggles.feature1.reset()
```

### ff.get()
Returns an object with all the flags and the values

> :warning: **Do not use FF within your code**. Instead, use featuresManager directly.

### ff.reset()
Go back to the default state. 