import { FeatureState } from "./feature-state";

class ModificationsState<T extends FeatureState> {
    private localstorageStateKey = 'FF_STATE';
    private modifications: Partial<T>;

    constructor() {
        this.modifications = this.readFromStorage();
    }

    public get(): Partial<T> {
        return this.modifications;
    }

    public setItem(key: keyof T, value: boolean): void {
        const currentValue = this.get();
        const newValue = { ...currentValue, [key]: value };
        this.set(newValue);
    }

    public resetItem(key: keyof T): void {
        const currentValue = this.get();
        const newValue = { ...currentValue };
        delete newValue[key];
        this.set(newValue);
    }

    public reset(): void {
        window.localStorage.removeItem(this.localstorageStateKey);
    }

    private set(data: Partial<T>): void {
        this.modifications = data;
        const json = JSON.stringify(data);
        window.localStorage.setItem(this.localstorageStateKey, json);
    }

    private readFromStorage(): Partial<T> {
        const stored = window.localStorage.getItem(this.localstorageStateKey);
        if (!stored) {
            return {};
        }
        try {
            const parsed = JSON.parse(stored);
            return parsed;
        } catch (e) {
            console.warn('stored state is not a json');
            return {};
        }
    }
}

export class FeaturesManager<T extends FeatureState> {
    private modificationsState = new ModificationsState<T>();

    constructor(private defaultState: T) {
    }

    public getState(): T {
        return { ...this.defaultState, ...this.modificationsState.get() };
    }

    public reset(): void {
        this.modificationsState.reset();
    }

    public setItem(key: keyof T, value: boolean): void {
        this.modificationsState.setItem(key, value);
    }

    public resetItem(key: keyof T): void {
        this.modificationsState.resetItem(key);
    }

    public getDefaults(): T {
        return this.defaultState;
    }

    public getModifications(): Partial<T> {
        return this.modificationsState.get();
    }
}
