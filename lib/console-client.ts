import { FeatureState } from "./feature-state";
import { FeaturesManager } from "./features-manager";

interface Toggle {
    on: () => void,
    off: () => void,
    get: () => boolean,
    reset: () => void,
}

const keyStyle = 'font-weight: bold';
const valueStyle = 'font-wight: normal';
const hintStyle = 'color: gray';

export class ConsoleClient<T extends FeatureState> {
    constructor(private manager: FeaturesManager<T>) {
        this.setClient();
    }

    private setClient() {
        (window as any)['ff'] = {
            toggles: this.getToggles(),
            reset: () => this.manager.reset(),
            get: () => this.manager.getState(),
            status: () => this.viewStatus(),
        };
    }

    private getToggles(): { [key in keyof T]: Toggle } {
        const setters: Partial<{ [key in keyof T]: Toggle }> = {};
        for (let key in this.manager.getState()) {
            setters[key] = this.getToggle(key);
        }
        return setters as { [key in keyof T]: Toggle };
    }

    public viewStatus(): void {
        const state = this.manager.getState();
        const defaults = this.manager.getDefaults();
        const modifications = this.manager.getModifications();

        const modified = Object.keys(modifications).filter(k => defaults.hasOwnProperty(k));
        const unmodified = Object.keys(defaults).filter(k => !modifications.hasOwnProperty(k));
        const redundantKeys = Object.keys(modifications).filter(k => !defaults.hasOwnProperty(k));

        console.group('Cheaper Flags Status');
        modified.forEach(key => {
            const value = state[key] ? 'on' : 'off';
            const originalValue = '(orioginal: ' + (defaults[key] ? 'on' : 'off') + ')';
            console.log(`%c${key}\t %c${value} %c${originalValue}`, keyStyle, valueStyle, hintStyle);
        });
        unmodified.forEach(key => {
            const value = state[key] ? 'on' : 'off';
            console.log(`%c${key}\t %c${value}`, keyStyle, valueStyle);
        });
        redundantKeys.forEach(key => {
            const value = state[key] ? 'on' : 'off';
            console.log(`%c${key}\t %c${value} %c(redundant)`, keyStyle, valueStyle, hintStyle);
        });
        console.groupEnd();
    }

    private getToggle(key: keyof T): Toggle {
        const on = () => this.manager.setItem(key, true);
        const off = () => this.manager.setItem(key, false);
        const get = () => this.manager.getState()[key];
        const reset = () => this.manager.resetItem(key);
        return { on, off, get, reset };
    }
}